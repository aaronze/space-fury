class Tower extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, texture, frame, options) {
        super(scene, x, y, 'tower', frame);
        this.nextFire = 0;
        this.selection = new Selection(scene);
    }

    buildFromTemplate(towerTemplate) {
        if (towerTemplate.texture) {
            this.setTexture(towerTemplate.texture);
        }
        this.towerReload = towerTemplate.towerReload;
        this.bulletTemplate = towerTemplate.bulletTemplate;
    }

    place(i, j) {
        let playSize = width - leftGutter - rightGutter;
        let gridSize = playSize / numberOfLanes;
        let topOfTowers = height - numberOfTowerRows * gridSize;

        this.x = leftGutter + i * gridSize + gridSize/2;
        this.y = topOfTowers + j * gridSize + gridSize/2;
    }

    fire() {
        let bullet = bullets.get();
        if (bullet) {
            bullet.buildFromTemplate(this.bulletTemplate);
            bullet.fire(this.x, this.y - this.displayHeight / 2.5, this.rotation - Math.PI / 2);
        }
    }

    update(time, delta) {
        if (time > this.nextFire) {
            this.fire();
            this.nextFire = time + this.towerReload;
        }
    }

    onSelect() {
        let distance = this.bulletTemplate.speed * this.bulletTemplate.timeToLive + this.displayHeight / 2.5;
        this.selection.isVisible = true;
        this.selection.redraw(this.x, this.y, distance);
    }

    offSelect() {
        this.selection.isVisible = false;
        this.selection.redraw(this.x, this.y, 0);
    }
}


