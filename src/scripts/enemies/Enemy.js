class Enemy extends Phaser.GameObjects.Image {
    constructor(scene, x, y, texture, options) {
        super(scene, x, y, 'enemy');
        this.follower = { t: 0, vec: new Phaser.Math.Vector2() };
        this.path = lanes[Phaser.Math.Between(0, lanes.length - 1)];
        this.healthBar = new HealthBar(scene, x, y, 60, 10, -30);
    }

    buildFromTemplate(enemyTemplate) {
        if (enemyTemplate.texture) {
            this.setTexture(enemyTemplate.texture);
        }
        this.maxHitpoints = enemyTemplate.hitpoints;
        this.hitpoints = enemyTemplate.hitpoints;
        this.enemySpeed = enemyTemplate.enemySpeed;
    }

    update(time, delta) {
        this.follower.t += this.enemySpeed * delta;
        this.path.getPoint(this.follower.t, this.follower.vec);
        this.setPosition(this.follower.vec.x, this.follower.vec.y);
        if (this.follower.t >= 1) {
            this.setActive(false);
            this.setVisible(false);
        }
        this.healthBar.redraw(this.follower.vec, this.hitpoints / this.maxHitpoints);
    }

    startOnPath() {
        this.follower.t = 0;
        this.path.getPoint(this.follower.t, this.follower.vec);
        this.setPosition(this.follower.vec.x, this.follower.vec.y);
    }

    receiveDamage(damage) {
        this.hitpoints -= damage;

        if (this.hitpoints <= 0) {
            this.hitpoints = 0;
            this.healthBar.clear();
            this.setVisible(false);
            this.setActive(false);
        }
    }
}
