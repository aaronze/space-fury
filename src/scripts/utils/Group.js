
class Group {
    constructor() {
        this.releaseInterval = 500;
        this.sizeMultiplier = 1;
    }

    buildFromTemplate(groupTemplate) {
        this.enemy = groupTemplate.enemy;
        if (typeof groupTemplate.releaseInterval !== "undefined") {
            this.releaseInterval = groupTemplate.releaseInterval;
        }
        if (typeof groupTemplate.sizeMultiplier !== "undefined") {
            this.sizeMultiplier = groupTemplate.sizeMultiplier;
        }

        this.remainingInterval = this.releaseInterval;
    }

    populate(size) {
        this.remaining = Math.floor(size * this.sizeMultiplier);
        if (this.remaining < 1) this.remaining = 1;
    }

    update(time, delta) {
        let timeInterval = delta;
        while (this.remainingInterval <= timeInterval && this.remaining > 0) {
            let enemy = enemies.get();
            if (enemy) {
                enemy.buildFromTemplate(this.enemy);
                enemy.setActive(true);
                enemy.setVisible(true);
                enemy.startOnPath();
                this.remaining -= 1;
            }
            timeInterval -= this.remainingInterval
            this.remainingInterval = this.releaseInterval;
        }

        if (this.remaining > 0) {
            this.remainingInterval -= timeInterval;
            return 0;
        }

        return timeInterval;
    }
}
