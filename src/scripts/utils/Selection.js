class Selection {
    constructor(scene) {
        this.selection = new Phaser.GameObjects.Graphics(scene);
        this.isVisible = false;

        scene.add.existing(this.selection);
    }

    redraw(x, y, radius) {
        this.selection.clear();

        if (this.isVisible) {
            this.selection.fillStyle(0xFFFFFF, 0.5);
            this.selection.fillCircle(x, y, radius);
        }
    }
}