/**
 * Control the health bar
 */
class HealthBar {

	/**
	 * Create the health bar
	 *
	 * @param {PhaserScene} scene
	 * @param {number} x
	 * @param {number} y
	 * @param {number} width
	 * @param {number} height
	 * @param {number} offset
	 */
    constructor(scene, x, y, width, height, offset) {

        this.healthBar = new Phaser.GameObjects.Graphics(scene);
        this.healthBarWidth = width;
        this.healthBarHeight = height;
        this.healthBarOffset = offset;

        scene.add.existing(this.healthBar);
    }

	/**
	 * Redraw the health bar
	 *
	 * @param {vector} position
	 * @param {number} fillPercent
	 */
    redraw(position, fillPercent) {

        this.healthBar.clear();

        let fillWidth = Math.floor(fillPercent * this.healthBarWidth);
        this.healthBar.fillStyle(0x33FF33, 1.0);
        this.healthBar.fillRect(position.x - this.healthBarWidth/2, position.y + this.healthBarOffset, fillWidth, this.healthBarHeight);
    }

	/**
	 * Clear the health bar
	 */
    clear() {

        this.healthBar.clear();
    }
}
