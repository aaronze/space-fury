var bulletTemplates = {
    standard: {
        damage: 50,
        speed: Phaser.Math.GetSpeed(600, 1),
        timeToLive: 500,
        texture: 'bullet'
    },
    laser: {
        damage: 2,
        speed: Phaser.Math.GetSpeed(600, 1),
        timeToLive: 300,
        texture: 'laser'
    }
};

var towerTemplates = {
    standard: {
        towerReload: 2000,
        bulletTemplate: bulletTemplates.standard
    },
    laser: {
        towerReload: 20,
        bulletTemplate: bulletTemplates.laser,
        texture: 'tower-laser'
    }
};

var enemyTemplates = {
    standard: {
        hitpoints: 150,
        enemySpeed: 0.0001
    },
    quick: {
        hitpoints: 75,
        enemySpeed: 0.0002
    },
    tanky: {
        hitpoints: 300,
        enemySpeed: 0.00005
    }
};

var groupTemplate = {
    standard: {
        enemy: enemyTemplates.standard,
        releaseInterval: 500
    },
    quick: {
        enemy: enemyTemplates.quick,
        releaseInterval: 500
    },
    tanky: {
        enemy: enemyTemplates.tanky,
        releaseInterval: 1000,
        sizeMultiplier: 0.5
    },
    swarm: {
        enemy: enemyTemplates.quick,
        releaseInterval: 150,
        sizeMultiplier: 3,
    },
    boss: {
        enemy: enemyTemplates.tanky,
        sizeMultiplier: 0,
        rewardMultiplier: 10
    },
    ambush: {
        enemy: enemyTemplates.standard,
        releaseInterval: 30
    }
};

var levelTemplates = {
    level_1: {
        groups: [groupTemplate.standard, groupTemplate.quick, groupTemplate.tanky, groupTemplate.standard, groupTemplate.boss],
        size: 10
    }
};