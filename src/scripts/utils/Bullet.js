class Bullet extends Phaser.GameObjects.Image {
    constructor(scene, x, y, texture, options) {
        super(scene, x, y, 'bullet');
        this.direction = new Phaser.Math.Vector2(0, 0);
        this.lifespan = 0;
        this.speed = Phaser.Math.GetSpeed(600, 1);
        this.damage = 50;
    }

    buildFromTemplate(bulletTemplate) {
        if (bulletTemplate.texture) {
            this.setTexture(bulletTemplate.texture);
        }
        this.damage = bulletTemplate.damage;
        this.speed = bulletTemplate.speed;
        this.timeToLive = bulletTemplate.timeToLive;
    }

    fire(x, y, angle) {
        this.setActive(true);
        this.setVisible(true);

        this.direction = new Phaser.Math.Vector2(Math.cos(angle), Math.sin(angle));
        this.lifespan = this.timeToLive;

        this.setPosition(x, y);
        this.setRotation(angle);
    }

    update(time, delta) {
        this.lifespan -= delta;

        this.x += this.direction.x * this.speed * delta;
        this.y += this.direction.y * this.speed * delta;

        if (this.lifespan <= 0) {
            this.setActive(false);
            this.setVisible(false);
        }
    }

    getDistance() {
        return this.timeToLive * this.speed;
    }

    damageOther(other) {
        if (other.active === true && this.active === true) {
            this.setActive(false);
            this.setVisible(false);
            other.receiveDamage(this.damage);
        }
    }
}
