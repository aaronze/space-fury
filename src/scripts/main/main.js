var width = 640;
var height = 512;
var lanes = [];

var config = {
	type: Phaser.AUTO,
	parent: 'content',
	width: width,
	height: height,
	physics: {
		default: 'arcade'
	},
	scene: {
		preload: preload,
		create: create,
		update: update,
		key: 'main'
	}
};

var game = new Phaser.Game(config);
var enemies;
var towers;
var bullets;
var level;
var buttons = [];

UI.build();

function preload() {
	this.load.image('button', 'assets/sprites/UI/button.png');
	this.load.image('button-hover', 'assets/sprites/UI/button-hover.png');
	this.load.image('button-pressed', 'assets/sprites/UI/button-pressed.png');
	this.load.image('enemy', 'assets/sprites/Enemy/enemy.png');
	this.load.image('bullet', 'assets/sprites/Bullets/bullet.png');
	this.load.image('laser', 'assets/sprites/Bullets/laser.png');
	this.load.spritesheet('tower', 'assets/sprites/Red/Weapons/turret_01_mk1.png', { frameWidth: 128, frameHeight: 128 });
	this.load.spritesheet('tower-laser', 'assets/sprites/Purple/Weapons/turret_01_mk1.png', { frameWidth: 128, frameHeight: 128 });
}

function create() {
	graphics = this.add.graphics();
	UI.draw(graphics);

	graphics.lineStyle(3, 0xffffff, 1);
	let playSize = width - leftGutter - rightGutter;
	let laneSize = playSize / numberOfLanes;
	for (let x = 0; x < numberOfLanes; x++) {
		let xPos = leftGutter + laneSize/2 + laneSize * x;
		let path = this.add.path(xPos, -32);
		path.lineTo(xPos, height + 32);
		path.draw(graphics);
		lanes.push(path);
	}

	this.anims.create({
		key: 'tower-idle',
		frames: [ { key: 'tower', frame: 0 } ],
		frameRate: 20
	});
	this.anims.create({
		key: 'tower-fire',
		frames: this.anims.generateFrameNumbers('tower', { start: 1, end: 7 }),
		frameRate: 10
	});

	// Generate the first level
	level = new Level();
	level.buildFromTemplate(levelTemplates.level_1);

	// Groups added in order of drawing
	towers = this.add.group({ classType: Tower, runChildUpdate: true });
	enemies = this.physics.add.group({ classType: Enemy, runChildUpdate: true });
	bullets = this.physics.add.group({ classType: Bullet, runChildUpdate: true });

	this.physics.add.overlap(enemies, bullets, (enemy, bullet) => bullet.damageOther(enemy));

	this.input.on('pointerdown', UI.clickGrid);

	waveText = this.add.text(10, 10, "Wave");
	waveTimerText = this.add.text(10, 30, "Next Wave:");
	remainingEnemiesText = this.add.text(10, 50, "Spawning:");

	UI.buildTowerButtons(this.scene.scene);
}

function update(time, delta) {
	if (level) {
		level.update(time, delta);
	}
}
