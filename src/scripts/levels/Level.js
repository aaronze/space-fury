class Level {
    constructor() {
        this.waveInterval = 7000;
        this.remainingInterval = 0;
    }

    buildFromTemplate(levelTemplate) {
        this.group = null;
        this.groups = levelTemplate.groups.reverse();
        this.size = levelTemplate.size;
        this.waves = this.groups.length;
    }

    nextGroup() {
        this.remainingInterval = this.waveInterval;
        this.group = new Group();
        this.group.buildFromTemplate(this.groups.pop());
        this.group.populate(this.size);
        this.updateUI();
    }

    getWave() {
        return this.waves - this.groups.length;
    }

    update(time, delta) {
        let timeInterval = delta;
        // Wait until the wave timer has expired before sending enemies
        if (this.remainingInterval > 0) {
            if (timeInterval < this.remainingInterval) {
                this.updateUI();
                this.remainingInterval -= timeInterval;
                return 0;
            }
            timeInterval -= this.remainingInterval;
            this.remainingInterval = 0;
        }

        // While we have time remaining, spawn enemies
        while (timeInterval > 0 && this.groups.length > 0) {
            // If our current group has run out of enemies, get the next group
            if (!this.group || this.group.remaining < 1) {
                this.nextGroup();
            }
            timeInterval = this.group.update(time, timeInterval);
            if (timeInterval > 0) {
                if (timeInterval < this.remainingInterval) {
                    this.updateUI();
                    this.remainingInterval -= timeInterval;
                    return 0;
                }
                timeInterval -= this.remainingInterval;
                this.remainingInterval = 0;
            }
        }

        this.updateUI();
    }

    updateUI() {
        waveText.setText("Wave: " + this.getWave());
        remainingEnemiesText.setText("Spawning: " + this.group.remaining);
        waveTimerText.setText("Next Wave: " + Math.floor(this.remainingInterval/1000));
    }
}