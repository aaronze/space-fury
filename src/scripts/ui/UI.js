var leftGutter = 100;
var rightGutter = 100;
var numberOfLanes = 5;
var numberOfTowerRows = 4;
var grid = [];
var graphics;
var selectedTower;
var placeTowerTemplate;

// UI Text
var waveText;
var waveTimerText;
var remainingEnemiesText;

var towerButtons;

var UI = {
    build: function() {
        // Generate the slots that can hold towers
        for (let y = 0; y < numberOfTowerRows; y++) {
            let row = [];
            for (let x = 0; x < numberOfLanes; x++) {
                row.push(0);
            }
            grid.push(row);
        }
    },

    buildTowerButtons: function(scene) {
        towerButtons = [];

        let tower1Button = new Button(scene, width - 50, 40, 60, 60, 'tower');
        tower1Button.onClick(() => this.selectTowerButton(tower1Button, towerTemplates.standard));
        buttons.push(tower1Button);
        towerButtons.push(tower1Button);

        let tower2Button = new Button(scene, width - 50, 120, 60, 60, 'tower-laser');
        tower2Button.onClick(() => this.selectTowerButton(tower2Button, towerTemplates.laser));
        buttons.push(tower2Button);
        towerButtons.push(tower2Button);
    },

    selectTowerButton: function(button, template) {
        console.log("Select button");
        towerButtons.forEach(function(towerButton) {
            if (towerButton !== button) {
                towerButton.losePress();
            }
        });

        placeTowerTemplate = template;
    },

    clickGrid: function(pointer) {
        let playSize = width - leftGutter - rightGutter;
        let gridSize = playSize / numberOfLanes;
        let topOfTowers = height - numberOfTowerRows * gridSize;

        let i = Math.floor((pointer.x - leftGutter) / gridSize);
        let j = Math.floor((pointer.y - topOfTowers) / gridSize);

        if (selectedTower) {
            selectedTower.offSelect();
            selectedTower = null;
        }

        if (UI.gridValid(i, j)) {
            if (UI.gridEmptyAt(i, j)) {
                // Has selected a tower to place
                if (placeTowerTemplate) {
                    let tower = towers.get();
                    if (tower) {
                        tower.buildFromTemplate(placeTowerTemplate);
                        tower.setActive(true);
                        tower.setVisible(true);
                        tower.place(i, j);
                        grid[j][i] = tower;
                    }
                }
            } else {
                selectedTower = grid[j][i];
                selectedTower.onSelect();
            }
        }
    },

    gridValid: function(i, j) {
        if (i < 0 || i >= numberOfLanes) return false;
        if (j < 0 || j >= numberOfTowerRows) return false;
        return true;
    },

    gridEmptyAt: function(i, j) {
        return this.gridValid(i, j) && grid[j][i] === 0;
    },

    draw: function(graphics) {
        this.drawGrid(graphics);
    },

    drawGrid: function(graphics) {
        graphics.lineStyle(1, 0x5555ff, 0.8);

        let playSize = width - leftGutter - rightGutter;
        let gridSize = playSize / numberOfLanes;
        for (let y = 0; y <= numberOfTowerRows; y++) {
            graphics.moveTo(leftGutter, height - y * gridSize);
            graphics.lineTo(width - rightGutter, height - y * gridSize);
        }
        for (let x = 0; x <= numberOfLanes; x++) {
            graphics.moveTo(leftGutter + x * gridSize, height - numberOfTowerRows * gridSize);
            graphics.lineTo(leftGutter + x * gridSize, height);
        }
        graphics.strokePath();
    }
};
