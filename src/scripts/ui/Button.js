class Button {
    constructor(scene, x, y, width, height, icon) {
        this.button = new Phaser.GameObjects.Image(scene, x, y, 'button').setInteractive();
        this.icon = new Phaser.GameObjects.Image(scene, x, y, icon);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.pressed = false;
        this.hover = false;

        scene.add.existing(this.button);
        scene.add.existing(this.icon);

        this.button.on('pointerover', () => this.onMouseEnter(this));
        this.button.on('pointerout', () => this.onMouseExit(this));
        this.button.on('pointerup', () => this.onMousePress(this));
    }

    onClick(callback) {
        this.clickCallback = callback;
    }

    onMouseEnter(button) {
        button.hover = true;
        button.redraw();
    }

    onMouseExit(button) {
        button.hover = false;
        button.redraw();
    }

    onMousePress(button) {
        button.pressed = !button.pressed;
        if (button.pressed) {
            if (this.clickCallback) {
                this.clickCallback();
            }
        }
        button.redraw();
    }

    redraw() {
        if (this.pressed) {
            this.button.setTexture('button-pressed')
        } else if (this.hover) {
            this.button.setTexture('button-hover')
        } else {
            this.button.setTexture('button')
        }
    }

    losePress() {
        if (this.pressed) {
            this.pressed = false;
            this.redraw();
        }
    }
}