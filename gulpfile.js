/**
 * Gulp tasks
 */

const { copy, mkdirp } = require('fs-extra');
const { readdir, rm } = require('fs/promises');
const { dest, src, series, watch } = require('gulp');
const gulpPlumber = require('gulp-plumber');
const plug = require('gulp-load-plugins')();
const pump = require('pump');

const config = require('./gulp-config.json');

/**
 * Build the distribution folder
 */
async function build(cb) {

	await mkdirp('./dist');
	await copy('./src/index.html', './dist/index.html');
	await mkdirp('./dist/assets');
	await copy('./assets', './dist/assets');
	await mkdirp('./dist/scripts');
	await copy('./src/libs', './dist/scripts');
	await scripts();
	cb();
}

/**
 * Clean the distribution folder
 */
async function clean(cb) {

	await rm('./dist', {recursive: true, force: true});
	cb();
}

/**
 * Start the live reload server
 */
function connect(cb) {

	// Start the server
	plug.connect.server({

		base: 'http://localhost',
		port: 5000,
		root: './dist',
		livereload: true,
	});

	pump([

		src('./dist'),

		// Open the browser
		plug.open({uri: 'http://localhost:5000'}),

	], cb);
}

/**
 * Build the scripts
 */
async function scripts(cb) {

	// Loop through the scripts folders
	var promises = [];
	var dir = './src/scripts';
	var folders = await readdir(dir);
	for(var folder of folders) {

		// Wait for the gulp task before moving to the next folder
		await ((folder) => {

			return new Promise((resolve, reject) => {

				console.info('build:', folder);
				pump([

					src(`${dir}/${folder}/**/*.js`),
					plug.jshint(config.jshint),
					plug.jshint.reporter('default'),
					plug.concat(folder + '.js'),
					//plug.uglify(),
					dest('./dist/scripts'),
				], resolve);
			});
		})(folder);
	}

	typeof cb == 'function' ? cb() : false;
}

/**
 * Create the watch list
 */
function watchFiles(cb) {

	// scripts
	watch('./src/scripts/**/*.js', series(scripts, reload));

	cb();
}

/**
 * Reload the server after update
 */
function reload(cb)
{
	console.info('reload site');
	pump([
		src('./dist'),
		plug.connect.reload()
	], cb);
}

// Gulp tasks
exports.clean = clean;
exports.build = build;
exports.default = series(clean, build);
exports.watch = series(clean, build, connect, watchFiles);
